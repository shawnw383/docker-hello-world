<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example. Test to see if 'Laravel' is present on the screen
     *
     * @return void
     */
    public function testBasicExample()
    {
        // Print what url is the chrome browser visiting
        fwrite(STDERR, print_r(env('APP_URL'), true));
        fwrite(STDERR, print_r(env('BASE_URL'), true));

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Laravel');
        });
    }

    /**
     * Dumps contents of root url to console
     *
     * @return void
     */
    public function testDump()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->dump();
        });
    }
}
